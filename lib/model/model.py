import numpy
import tensorflow as tf
from tensorflow.keras import layers

class Conv( layers.Layer ):
    def __init__( self, filters: int, kernel_size: tuple, **kwargs ):
        self.filters = filters
        self.kernel_size = kernel_size

        super( Conv, self ).__init__( **kwargs )

    def build( self, input_shape ):
        spatial_dims = len( self.kernel_size )
        all_dims = len( input_shape )
        assert all_dims == spatial_dims + 2
        kernel_shape = self.kernel_size + ( input_shape[-1], self.filters )
        bias_shape = tuple( 1 for _ in range( all_dims + 1 ) ) + ( self.filters, )
        self.kernel = self.add_weight(
                name = "kernel",
                shape = kernel_shape,
                initializer = "uniform",
                trainable = True
                )
        self.bias = self.add_weight(
                name = "bias",
                shape = bias_shape,
                initializer = "zeros",
                trainable = True
                )
        self.built = True
    def call( self, inputs ):
        ( b, wi, zi, yi, xi, c ) = inputs.get_shape().as_list()
        ( wk, zk, yk, xk ) = self.kernel_size
        wo = wi - wk + 1
        frame_res = [ None ] * wo
        for i in range( wk):
            for j in range( wi ):
                out_frame = j - ( i - wk//2 ) - ( wi - wo )//2
                if out_frame < 0 or out_frame >= wo:
                    continue
                frame_conv3d = tf.nn.convolution( tf.reshape( inputs[:,:,j,:,:], ( b, zi, yi, xi, c ) ), self.kernel[:,:,:,i], padding = self.padding )
                if frame_res[ out_frame ] is None:
                    frame_res[ out_frame ] = frame_conv3d
                else:
                    frame_res[ out_frame ] += frame_conv3d

        output = tf.stack( frame_res, axis = 2 )

        if self.activation:
            output = self.activation( output )
        
        return output
    def compute_output_shape( self, input_shape ):
        sizes = input_shape[1:-1]
        if self.padding.lower() == "valid":
            sizes = [ s - ksize + 1 for s, ksize in zip( sizes, self.kernel_size ) ]
        return input_shape[:1] + sizes + ( self.filters, )

class Model( tf.keras.Model ):
    def __init__( self, vocab_size: int = 1000, max_len: int = 1000 ):
        super( Model, self ).__init__()
        self.model = tf.keras.Sequential([
            layers.Conv3D( 128, (6,6,6), name = "conv1" ),
            layers.MaxPooling3D(),
            layers.ReLU(),
            layers.Conv3D( 128, (3,3,3), name = "conv2" ),
            layers.MaxPooling3D(),
            layers.Softmax(),
            layers.ConvLSTM2D( 64, (2,3), return_sequences = True, name = "decoder" ),
            layers.Flatten(),
            layers.Dense( 10*vocab_size, actiation = "relu" ),
            layers.Reshape( ( -1, vocab_size ) ),
            layers.Dense( vocab_size, activation = "softmax" )
            ])
        self.max_len = max_len
        return
    
    def call( self, inputs ):
        return self.model( inputs )
    def predict( self, inputs ):
        x = self.call( inputs )
        return tf.argmax( x, axis = -1 )

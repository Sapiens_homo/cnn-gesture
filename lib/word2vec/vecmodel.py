import gensim
import gensim.downloader
import gensim.test.utils
import numpy as np

class vecmodel:
    def __init__( self, path: str = "" ):
        if len(path):
            self.vec = gensim.models.KeyedVectors.load_word2vec_format( gensim.test.utils.datapath(path), binary=True )
        else:
            self.vec = gensim.downloader.load( list(gensim.downloader.info()["models"].keys())[0] )
            #self.vec = gensim.downloader.load( list( gensim.downloader.info()["corpora"] )[9] )
        return
    def __len__( self ):
        return len( self.vec )
    def to_vec( self, src: str ):
        a = []
        max_seq_len = 0
        for i in src.split():
            a.append( self.vec.get_vector( i ) )
            max_seq_len = max( [ max_seq_len, len( self.vec.get_vector( i ) ) ] )
        ret = np.zeros( ( len( a ), max_seq_len ) )
        for i, seq in enumerate( a ):
            for j, token in enumerate( seq ):
                ret[ i, j ] = token
        return ret
    def to_str( self, src: list ):
        ret = ""
        for _ in src:
            ret += self.vec.most_similar( [_], topn = 1 )[0][0] + " "
        return ret

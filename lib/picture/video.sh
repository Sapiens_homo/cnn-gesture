#!/bin/bash

echo read $1 extract to directory $2

if [ -z '$2' ] || [ -z '$1' ]
then
	echo Miss file or directory
	echo exit.
	exit
fi

ls $2 > /dev/null
if [ $? -ne 0 ]
then
	mkdir $2
fi

ffmpeg -i $1 $2/%d.png

mkdir tmp

for i in $(ls $2)
do
	ffmpeg -i $2/$i -vf scale=640:640 tmp/$i
done
mv tmp/* $2/
rm -r tmp

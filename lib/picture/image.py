from PIL import Image
import tensorflow as tf

import os

class image:
    def __init__( self, dir_path: str ):
        self.index = 0
        self.files = os.listdir( dir_path )
        self.files.sort()
        self.prefix = dir_path
        return
    def get( self ):
        if self.index < len( self.files ):
            img = Image.open( self.prefix + "/" + self.files[ self.index ] )
            img = tf.keras.preprocessing.image.array_to_img( img )
            img = tf.keras.preprocessing.image.img_to_array( img )
            self.index += 1
            return tf.convert_to_tensor( img, dtype = tf.float32 )
        self.index = 0
        return None
def get_img( path: str ):
    ret = Image.open( path )
    ret = tf.keras.preprocessing.image.array_to_img( ret )
    ret = tf.keras.preprocessing.image.img_to_array( ret )
    return tf.convert_to_tensor( ret, dtype = tf.float32 )

import cv2
import tensorflow as tf

class Camera:
    def __init__( self, width: int = 640, height: int = 640 ):
        self.wid = width
        self.hei = height
        return
    def __enter__( self ):
        cap = cv2.VideoCapture( 1 )
        if not cap.isOpened():
            __exit__()
            return
        ret = True
        while ret:
            ret, frame = cap.read()
            if ret:
                frame = cv2.resize( frame, tuple( self.wid, self.hei ) )
        return
    def __exit__( self, *arg ):
        return

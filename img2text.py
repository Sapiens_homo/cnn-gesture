import lib.word2vec as words
import lib.picture as picture

import argparse
import numpy as np
import tensorflow as tf
import datetime

import os

def new_model( out: tuple = (10,300) ):
    ret = tf.keras.Sequential([
        tf.keras.layers.Conv2D( 128, 6, name = "conv1" ),
        tf.keras.layers.MaxPooling2D(),
        tf.keras.layers.Conv2D( 128, 3, name = "conv2" ),
        tf.keras.layers.MaxPooling2D(),
        tf.keras.layers.Flatten(),
        tf.kreas.layers.Dense( out[0] * out[1], name = "r1" ),
        tf.keras.layers.Reshape( (-1, out[1] ) ),
        tf.keras.layers.Dense( out[1], "softmax", name = "output" )
        ])
    ret.compile( optimizer = "adam", loss = "categorical_crossentropy" )
    return ret

if __name__ == "__main__":
    model_path = "./model"
    vec_path = ""
    img_path = "./imgs"
    parser = argparse.ArgumentParser()
    parser.add_argument( "--list", default = False, action = "store_true", help = "List devices" )
    parser.add_argument( "--img", default = img_path, help = "Images for trainning" )
    parser.add_argument( "--vec", default = vec_path, help = "Vector to store vocabulary" )
    parser.add_argument( "--output", default = model_path, help = "Output file, use with --train" )
    parser.add_argument( "--model", default = model_path, help = "Model path" )
    parser.add_argument( "--target", default = "", help = "Exceped sentences/words, words will be splited with spaces(%%20)" )
    args = parser.parse_args()
    if args.list:
        i = 0
        for _ in tf.config.list_logical_devices():
            print( str(i)+")", _ )
            i += 1
        exit( 0 )
    model_out = args.output
    model_in = args.model
    vec_path = args.vec
    img_path = args.img
    target = args.target
    dictionary = words.vecmodel.vecmodel( vec_path )
    vocab_size = len( dictionary )
    batch_size = 16
    epochs = 64
    print( "has", vocab_size, "vocabulary" )

    if not len(target):
        target = input( "Target: " )

    target = dictionary.to_vec( target )
    img = picture.get_img( img_path )

    model = new_model( target.shape )

    model.fit( img, target, batch_size = batch_size, callbacks = callbacks )

    model.save( model_out, save_format = "tf", include_optimizer = True )

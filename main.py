#!/bin/env python3
import lib.model as model
import lib.picture as picture
import lib.word2vec as words

#import lib.gui as gui

import argparse 
import numpy as np
import tensorflow as tf

import datetime

import os

if __name__ == "__main__":
    vacab_size = 1000
    max_length = 50
    model_path = "./model"
    vec_path = ""
    img_path = "./imgs"
    parser = argparse.ArgumentParser()
    parser.add_argument( "--device", default = 0, type = int, help = "Designate Specific Device to use" )
    parser.add_argument( "--list", default = False, action = "store_true", help = "List devices" )
    parser.add_argument( "--train", default = False, action = "store_true", help = "Train" )
    parser.add_argument( "--img", default = img_path, help = "Images for trainning" )
    parser.add_argument( "--vec", default = vec_path, help = "Vector to store vocabulary" )
    parser.add_argument( "--output", default = model_path, help = "Output file, use with --train" )
    parser.add_argument( "--model", default = model_path, help = "Model path" )
    parser.add_argument( "--continues", default = False, action = "store_true", help = "Save optimizer state" )
    parser.add_argument( "--target", default = "", help = "Exceped sentences/words, words will be splited with spaces(%%20)" )
    args = parser.parse_args()
    if args.list:
        i = 0
        for _ in tf.config.list_logical_devices():
            print( str(i)+")", _ )
            i += 1
        exit( 0 )
    device = args.device
    model_out = args.output
    model_in = args.model
    TRAIN = args.train
    vec_path = args.vec
    img_path = args.img
    save_state = args.continues
    dictionary = words.vecmodel.vecmodel( vec_path )
    vocab_size = len( dictionary )
    print( "has", vocab_size, "vocabulary" )
    
    mod = model.model.Model( (dictionary.to_vec( "hello" )).shape[1] )
    if not TRAIN:
        mod.model = tf.keras.models.load_model( model_in )
    mod.compile( optimizer = "adam", loss = "categorical_crossentropy" )
    _ = len( tf.config.list_logical_devices() )
    if( device >= _  or device < 0 ):
        print( f"Error: Device {device} out of range" )
        exit(1)
    with tf.device( tf.config.list_logical_devices()[device][0] ):
        if( TRAIN ):
            batch_size = 32
            epochs = 5
            IMG = picture.image( img_path )
            '''
            _ = IMG.get()
            __ = [ len(IMG.files) ]
            for ___ in np.shape(_):
                __.append(___)
            images = np.zeros( tuple(__) )
            images[0] = _
            for i in range( 1, len(IMG.files) ):
                images[i] = IMG.get()
            images = np.array( images )
            '''
            images = []
            while( ( i := IMG.get() ) != None ):
                images.append( i )

            if len( args.target ):
                right_seq = args.target
            else:
                right_seq = input()
            right_seq = dictionary.to_vec( right_seq )
            images = np.array( images )
            inputs = [ images, right_seq ]
            print( "shape of images:", np.shape( images ), "shape of sequences:", np.shape( right_seq ) )

            log_dir = "logs/fit/" + datetime.datetime.now().strftime( "%Y%m%d-%H%M%S" )
            tensorboard_callback = tf.keras.callbacks.TensorBoard( log_dir = log_dir, histogram_freq = 1 )
            mod.fit( images, right_seq, batch_size = batch_size, epochs = epochs, callbacks = [tensorboard_callback] )
            print( "start trainning" )
            if( save_state ):
                mod.model.save( model_out, save_format = "tf", include_optimizer = True )
            else:
                tf.keras.models.save_model( mod.model, model_out, save_format = "tf" )
        else:
            if len( img_path ):
                cam = picture.image( img_path )
            else:
                cam = picture.camera()
            imgs = cam.get()
            while imgs != None:
                seq = dictionary.to_str( [ mod.predict( imgs ) ] )
                print( seq )
                imgs = cam.get()

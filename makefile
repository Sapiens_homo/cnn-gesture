.PHONY: all clean run train
FILES = $(shell find . -name "__pycache__")
all: run
clean:
	@echo cleaning...
	@-rm -r $(FILES)
	@echo done
run:
	./main.py
train:
	./main.py --train

import lib.word2vec.vecmodel as model
import lib.picture.image as image

'''
mod = model.vecmodel()
print( len( mod ) )

a = mod.to_vec( "Hello" )
b = mod.to_vec( "world" )
c = mod.to_vec( "Hello world" )
print( a.shape )
print( b.shape )
print( c.shape )
'''
imgs = image( input() )

i = []
_ = imgs.get()
while( _ != None ):
    i.append( _ )
print( len( i ) == len( imgs.files ) )
